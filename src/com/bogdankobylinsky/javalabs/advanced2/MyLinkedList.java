package com.bogdankobylinsky.javalabs.advanced2;

/**
 * My implementation of <link>java.util.LinkedList</link>.
 *
 * @author Bogdan Kobylinsky
 */
public class MyLinkedList implements MyList {

    /**
     * Actual size of the list.
     */
    private int size;
    /**
     * First element in the list.
     */
    private ListItem first;
    /**
     * Last element in the list.
     */
    private ListItem last;

    /**
     * Adding <b>element</b> to the end of the List.
     *
     * @param element is a refernece to an Object.
     */
    @Override
    public void add(Object object) {
        if (first == null) {
            first = new ListItem(object, null, null);
        } else {
            if (last == null) {
                last = first.next = new ListItem(object, first, null);
            } else {
                last = last.next = new ListItem(object, last, null);
            }
        }
        size++;
    }

    /**
     * Adding <b>element</b> to the list in specified <b>index</b> position.
     *
     * @param index is the position for inserting <b>element</b>.
     * @param element is a reference to an Object.
     * @throws IllegalArgumentException if the specified index is not in the
     * legal range.
     */
    @Override
    public void add(int index, Object element) throws IllegalArgumentException {
        checkForIndex(index);
        if (index == 0) {
            ListItem newItem = new ListItem(element, first, null);
            first.prev = newItem;
            newItem.next = first;
            first = newItem;
        } else {
            ListItem replacable = first;
            for (int i = 0; i < index; i++) {
                replacable = replacable.next;
            }
            replacable.prev = replacable.prev.next = new ListItem(element, replacable.prev, replacable);
        }
        size++;
    }

    /**
     * Adding objects to the end of the list.
     *
     * @param newObjects is the array of objects to be inserted.
     * @throws NullPointerException if the array of added objects is not
     * initialized.
     */
    @Override
    public void addAll(Object[] newObjects) throws NullPointerException {
        if (newObjects == null) {
            throw new NullPointerException("Array of added elements must be "
                    + "initialized.");
        }

        if (newObjects.length <= 0) {
            return;
        }

        for (Object object : newObjects) {
            add(object);
        }
    }

    /**
     * Adding array of objects <b>newObjects</b> in specified <b>index</b>
     * position.
     *
     * @param index is the position for inserting array.
     * @param newObjects is the array of objects to be inserted.
     * @throws IllegalArgumentException if the specified index is not in the
     * legal range.
     * @throws NullPointerException if the array of added objects is not
     * initialized.
     */
    @Override
    public void addAll(int index, Object[] newObjects)
            throws IllegalArgumentException, NullPointerException {
        checkForIndex(index);
        if (newObjects == null) {
            throw new NullPointerException("Array of added elements must be "
                    + "initialized.");
        }

        if (newObjects.length <= 0) {
            return;
        }

        if (index == size - 1) {
            addAll(newObjects);
            return;
        }

        ListItem replecable = first;
        for (int i = 0; i < index; i++) {
            replecable = replecable.next;
        }
        ListItem lastBeforeInsert = last;

        for (Object object : newObjects) {
            add(object);
        }

        replecable.prev.next = lastBeforeInsert.next;
        lastBeforeInsert.next.prev = replecable.prev;
        last.next = replecable;
        replecable.prev = last;
        lastBeforeInsert.next = null;
    }

    /**
     * Getting the link to the object that corresponds to the specified
     * <b>index</b> position in the list.
     *
     * @param index is the position of object in array.
     * @return link to the object that is in <b>index</b> position in the array.
     * @throws IllegalArgumentException if the specified index is not in the
     * legal range.
     */
    @Override
    public Object get(int index) throws IllegalArgumentException {
        checkForIndex(index);
        ListItem returnable = first;
        for (int i = 0; i < index; i++) {
            returnable = returnable.next;
        }
        return returnable.object;
    }

    /**
     * Removing the object from the list that is in <b>index</b> position.
     *
     * @param index is the posiiton of object.
     * @return the removed object.
     * @throws IllegalArgumentException if the specified index is not in the
     * legal range.
     */
    @Override
    public Object remove(int index) throws IllegalArgumentException {
        checkForIndex(index);

        ListItem removable;
        if (index == 0) {
            removable = first;
            first.next.prev = null;
            first = first.next;
        } else if (index == size - 1) {
            removable = last;
            last.prev.next = null;
            last = last.prev;
        } else {
            removable = first;
            for (int i = 0; i < index; i++) {
                removable = removable.next;
            }
            removable.prev.next = removable.next;
            removable.next.prev = removable.prev;
        }
        size--;
        return removable.object;
    }

    /**
     * Setting the object <b>element</b> in specified <b>index</b> position.
     *
     * @param index is the position of the object to be setted.
     * @param element is the new object that wil be setted in <b>index</b>
     * position.
     * @throws IllegalArgumentException if the specified index is not in the
     * legal range.
     */
    @Override
    public void set(int index, Object object) throws IllegalArgumentException {
        checkForIndex(index);
        ListItem settable = first;
        for (int i = 0; i < index; i++) {
            settable = settable.next;
        }
        settable.object = object;
    }

    /**
     * Getting the first index of Object <b>object</b> that is in the list.
     *
     * @param object is the desired object.
     * @return the index of the desired object in the list.
     */
    @Override
    public int indexOf(Object object) {
        ListItem current = first;
        if (object == null) {
            for (int i = 0; i < size; i++) {
                if (current.object == null) {
                    return i;
                }
                current = current.next;
            }
        } else {
            for (int i = 0; i < size; i++) {
                if (current.object.equals(object)) {
                    return i;
                }
                current = current.next;
            }
        }
        return -1;
    }

    /**
     * Getting the actual size of the list.
     *
     * @return the actual size of the list.
     */
    @Override
    public int size() {
        return size;
    }

    /**
     * Getting the list in the array representation.
     *
     * @return the array of objects.
     */
    @Override
    public Object[] toArray() {
        Object[] items = new Object[size];
        ListItem current = first;
        for (int i = 0; i < size; i++) {
            items[i] = current.object;
            current = current.next;
        }
        return items;
    }

    /**
     * Checking whether specified <b>index</b> is in the range between <b>0</b>
     * and the <b>size</b> of the list.
     *
     * @param index is the specified index of the element in the list.
     * @throws IllegalArgumentException if the specified index is not in the
     * legal range.
     */
    private void checkForIndex(int index) throws IllegalArgumentException {
        if (index < 0 || index >= size) {
            throw new IllegalArgumentException("The index of specified element "
                    + "must be in the range between 0 and the size of the "
                    + "list.");
        }
    }

    /**
     * The class defines a specific separate element of the linked list.
     */
    private class ListItem {

        /**
         * Content of the item.
         */
        Object object;
        /**
         * Link to the next element in the list.
         */
        ListItem next;
        /**
         * Link to the previous element in the list.
         */
        ListItem prev;

        /**
         * Specifying the content of the item, with the links to the neighboring
         * iems.
         *
         * @param object is the content of the item.
         * @param prev is the link to the previous item.
         * @param next is the link to the next item.
         */
        ListItem(Object object, ListItem prev, ListItem next) {
            this.object = object;
            this.next = next;
            this.prev = prev;
        }
    }
}
