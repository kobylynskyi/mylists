package com.bogdankobylinsky.javalabs.advanced2;

/**
 * Analogue of the <link>java.util.List<link> collection.
 *
 * @author Bogdan Kobylinsky
 */
public interface MyList {

    /**
     * Adding <b>element</b> to the end of the List.
     *
     * @param element is a refernece to an Object.
     */
    void add(Object element);

    /**
     * Adding <b>element</b> to the list in specified <b>index</b> position.
     *
     * @param index is the position for inserting <b>element</b>.
     * @param element is a reference to an Object.
     */
    void add(int index, Object element);

    /**
     * Adding objects to the end of the list.
     *
     * @param c is the array of objects to be inserted.
     */
    void addAll(Object[] c);

    /**
     * Adding array of objects <b>c</b> in specified <b>index</b> position.
     *
     * @param index is the position for inserting array.
     * @param c is the array of objects to be inserted.
     */
    void addAll(int index, Object[] c);

    /**
     * Getting the link to the object that corresponds to the specified
     * <b>index</b> position in the list.
     *
     * @param index is the position of object in array.
     * @return link to the object that is in <b>index</b> position in the array.
     */
    Object get(int index);

    /**
     * Removing the object from the list that is in <b>index</b> position.
     *
     * @param index is the posiiton of object.
     * @return the removed object.
     */
    Object remove(int index);

    /**
     * Setting the object <b>element</b> in specified <b>index</b> position.
     *
     * @param index is the position of the object to be setted.
     * @param element is the new object that wil be setted in <b>index</b>
     * position.
     */
    void set(int index, Object element);

    /**
     * Getting the first index of Object <b>object</b> that is in the list.
     *
     * @param object is the desired object.
     * @return the index of the desired object in the list.
     */
    int indexOf(Object object);

    /**
     * Getting the actual size of the list.
     *
     * @return the actual size of the list.
     */
    int size();

    /**
     * Getting the list in the array representation.
     *
     * @return the array of objects.
     */
    Object[] toArray();
}
